from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from .views import *

urlpatterns = [
    path('', inicio, name='index'),
    path('create/', crear_contacto, name='crear'),
    path('editar/<int:id>/', editar_contacto, name='editar'),
    path('eliminar/<int:id>/', eliminar_contacto, name='eliminar')
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)