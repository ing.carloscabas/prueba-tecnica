from django.db import models

# Create your models here.
class Contacto(models.Model):

    def path(instance, filename):
        url='fotos/'+str(instance.celular)+'/'+str(filename)
        print(url)
        return url

    id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length=50, )
    apellido = models.CharField(max_length=50)
    celular = models.IntegerField(unique=True)
    email = models.EmailField()
    foto = models.ImageField(upload_to=path, null=True, blank=True)

    def __str__(self):
        return self.nombre