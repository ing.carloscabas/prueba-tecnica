from django.shortcuts import render, redirect
from django.http import HttpResponse, request
from .models import Contacto
from .forms import ContactoForm
from django.core.files.storage import FileSystemStorage
# Create your views here.
def inicio(request):
    contactos = Contacto.objects.all()
    contexto = {
        'contactos':contactos
    }
    return render(request, 'ver_contactos.html', contexto)


def saveImg(request):
    file = request.FILES['foto']
    f = FileSystemStorage()
    path = "fotos/" + str(request.POST.get("celular")) + "/" + file.name
    aux = f.save(path, file)

def crear_contacto(request):
    if request.method == 'GET':
       form = ContactoForm()
       contexto = {
          'formulario':form
       }
    else:
        form = ContactoForm(request.POST)
        contexto = {
            'formulario': form
        }
        if form.is_valid():
            form.save()
            saveImg(request)
            return redirect('index')
    return render(request, 'create.html', contexto)


def editar_contacto(request, id):
    product = Contacto.objects.get(id = id)
    if request.method == 'GET':
        form = ContactoForm(instance=product)
        print(form)
        contexto = {
            'formulario':form
        }
    else:
        file = request.FILES['foto']
        f = FileSystemStorage()
        path = "fotos/" + str(request.POST.get("celular")) + "/" + file.name
        form = ContactoForm(request.POST, instance=product)
        contexto = {
            'formulario':form
        }
        if form.is_valid():
            form.save()
            saveImg(request)
            return redirect('index')
    return render(request, 'create.html', contexto)

def eliminar_contacto(request, id):
    product = Contacto.objects.get(id=id)
    product.delete()
    return redirect('index')